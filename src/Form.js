import React, { Component } from 'react'
export default class AddForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 'Add a coupon!'
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        alert('Coupon added: ' + this.state.value);
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div class="input-group">
                    <label for="cname">Coupon Name</label>
                    <input type="text" name="cname" id="cname" class="input-box" placeholder="eg. Black Friday sale!"
                        maxlength="100" required />
                </div>
                <div class="input-group">
                    <label for="coupon-code">Coupon code</label>
                    <input type="text" name="coupon-code" id="coupon-code" class="input-box" placeholder="eg. BLACKFRIDAY20"
                        maxlength="10" />
                </div>
                <div class="input-group">
                    <label for="sale">Sale (percentage off)</label>
                    <input type="number" name="budget" id="budget" class="input-box" placeholder="eg. 20, 50, 5" />
                </div>
                <div class="input-group">
                    <label for="category">Coupon category</label>
                    <input type="text" name="category" id="category" class="input-box" placeholder="eg. FOOD, COFFEE, SHOPPING" />
                </div>
                <input type="submit" value="Add Coupon" class="btn btn-reverse" />

            </form>
        );
    }
}