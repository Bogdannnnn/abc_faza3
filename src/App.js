import React, { Component } from 'react';

import './App.css';
import axios from 'axios'
import Search from './Search'
import Form from './Form'

class App extends Component {

	constructor(props) {
		super(props);

		this.state = {
			latlong: "",
			venues: []
		};
	}

	componentDidMount() {
		this.getLocation();
	}
	getLocation = () => {
		navigator.geolocation.getCurrentPosition(response => {
			this.setState({
				latlong: response.coords.latitude + " , " + response.coords.longitude
			}, () => {
				this.getVenues("tech")
			});
		});
	};

	getVenues = (query) => {
		const client_id = "15WIGV3I4G5UQXMNAVJMWDIGQ3URWYQQD0ZXB0EUD1FZLPDA";
		const client_secret = "FNDBJD4U0TLDM1WGJZMQ2LTY0BCQFHBZAQUSAFUUOUBLPHJH";
		const endPoint = `https://api.foursquare.com/v2/venues/explore?client_id=${client_id}&client_secret=${client_secret}&v=20180323&near=Bucuresti&query=${query}`;
		const param = {
			//client_id: " Y3FW1FY5QAWHH25QAGGLDSVABJ243SE0HCGRCGFCUWU2G44Z ",
			// client_secret: " YJHRGKGNIJ12ANXIJCXOWVUUKVLNXFXHGW4IQ4EIJAKPJL11 ",
			client_id: "15WIGV3I4G5UQXMNAVJMWDIGQ3URWYQQD0ZXB0EUD1FZLPDA",
			client_secret: "FNDBJD4U0TLDM1WGJZMQ2LTY0BCQFHBZAQUSAFUUOUBLPHJH",

			ll: this.state.latlog,
			query: query,
			// v: " 20182507"

		};

		axios.get(endPoint).then(response => {
			this.setState({ venues: response.data.response.groups[0].items })
		});
	}

	render() {
		return (
			<div>
				<header>
					<h2><a href="index.html">
						Coupon Manager</a></h2>
					<nav>
						<ul>
							<li>
								<a href="index.html">Home</a>
							</li>
							<li>
								<a href="index.html">Add Coupon</a>
							</li>
						</ul>
					</nav>
				</header>

				<section id="search" class="search-wrap">
					<h1>Find A Location by Category</h1>
					

						<Search getVenues={this.getVenues} />
						<ul>
							{this.state.venues.map(venue => {
								return <li>{venue.venue.name} </li>
							})}
						</ul>
				
				</section>
				<section id="add" class="form-wrap">
					<h1>Add A Coupon</h1>
					<Form />
				</section>
			</div>
		)
	}
}

export default App;
