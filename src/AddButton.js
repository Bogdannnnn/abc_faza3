import React, { Component } from 'react'
export default class AddButton extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        return (
            <div>
                <section id="add" class="container">
                    <div class="form-wrap">
                        <form>
                            <div class="input-group">
                                <label for="cname">Coupon Name</label>
                                <input type="text" name="cname" id="cname" class="input-box" placeholder="eg. Black Friday sale!"
                                    maxlength="100" required />
                            </div>
                            <div class="input-group">
                                <label for="coupon-code">Coupon code</label>
                                <input type="text" name="coupon-code" id="coupon-code" class="input-box" placeholder="eg. BLACKFRIDAY20"
                                    maxlength="10" />
                            </div>
                            <div class="input-group">
                                <label for="sale">Sale (percentage off)</label>
                                <input type="number" name="budget" id="budget" class="input-box" placeholder="eg. 20, 50, 5" />
                            </div>
                            <div class="input-group">
                                <label for="category">Coupon category</label>
                                <input type="text" name="category" id="category" class="input-box" placeholder="eg. FOOD, COFFEE, SHOPPING" />
                            </div>
                            <input type="submit" value="Add Coupon" class="btn btn-reverse" />
                        </form>
                    </div>
                </section>
            </div>
        )
    }
    render() {
        return <button onClick={this.handleClick}>Add Coupon</button>;
    }
}